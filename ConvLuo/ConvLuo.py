# -*- coding: utf-8 -*-
"""
Created on Sun Apr 17 14:20:14 2016

@author: johnlakness
"""

print("Fetching data from MySQL connector")
# download http://www.iedb.org/doc/iedb_public.tar.gz and unpack into your mysql data directory
import mysql.connector
cnx = mysql.connector.connect(user='root', password='root',database='iedb_public')
cursor = cnx.cursor()
sql = """
select distinct
  mol1_seq epitope,
  left(sequence,180) mhc,
  as_num_value
from mhc_bind
join curated_epitope using(curated_epitope_id)
join  object on e_object_id=object_id
join  mhc_allele_restriction on DISPLAYED_RESTRICTION = MHC_ALLELE_NAME
join  source on chain_i_source_id=source_id
join assay_type on assay_type_id=as_type_id
where char_length(mol1_seq)=9 
and as_char_value <> 'Positive'
and units='nM'
and mhc_allele_name like "HLA-A%"
;
"""
cursor.execute(sql)
items = cursor.fetchall()
cursor.close()
cnx.close()

print "Converting amino acid sequences to one-hot"
import numpy as np
import pandas as pd
import operator as op

selected = [5,7,9,45,59,63,65,66,67,69,70,73,76,77,80,81,84,97,99,114,116,13,143,146,147,150,152,155,156,159,163,167,171]
aa = pd.read_table("../data/aminos.txt",header=None,sep="\s{2,}",engine='python').iloc[:,2]
A = np.zeros((len(items),9+len(selected),20),dtype=np.int8) #initialize the one-hot array
reduce( # set the amino acid positions
  lambda x,l: A.itemset(l,1),
  [(i,j,a) for i,item in enumerate(items) for j,a in enumerate(aa.searchsorted(map(chr,item[0])+list(op.itemgetter(*selected)(item[1]))))],
  None)
X = A.view() # reshape without copy
X.shape = (len(items),(9+len(selected))*20)
y = np.transpose(1-np.log(np.array([item[2] if item[2]>0 else 50000 for item in items],dtype=float,ndmin=2))/np.log(50000))
tts = np.random.binomial(1,0.5,len(items))==1

print "Setting up Neon"
import neon
import neon.util.argparser
import neon.initializers
import neon.layers
import neon.optimizers
import neon.models
import neon.transforms
import neon.callbacks

parser = neon.util.argparser.NeonArgparser(__doc__)
args = parser.parse_args()

neon.backends.gen_backend(backend='cpu', batch_size=128)

train = neon.data.ArrayIterator(X=X[tts,:], y=y[tts],lshape=(1,9+len(selected),20), make_onehot=False)
test = neon.data.ArrayIterator(X=X[~tts,:], y=y[~tts],lshape=(1,9+len(selected),20), make_onehot=False)

print "Fitting model"
init_norm = neon.initializers.Xavier() #Gaussian(loc=0.0, scale=0.01)
cost = neon.layers.GeneralizedCost(costfunc=neon.transforms.SumSquared())
optimizer = neon.optimizers.Adam()
activation = neon.transforms.Rectlin()
layers = [
  neon.layers.Conv((1,20,40),strides={'str_h':1,'str_w':20},init=neon.initializers.Orthonormal(), bias=neon.initializers.Constant(0),activation=neon.transforms.Identity(), name = "AAProps"),
#  neon.layers.Affine(nout=100,init=init_norm, bias=neon.initializers.Constant(0),activation=activation),
#  neon.layers.Dropout(keep=0.5), 
#  neon.layers.Affine(nout=100,init=init_norm, bias=neon.initializers.Constant(0),activation=activation),
#  neon.layers.Dropout(keep=0.5), 
#  neon.layers.Affine(nout=100,init=init_norm, bias=neon.initializers.Constant(0),activation=activation),
#  neon.layers.Dropout(keep=0.5),  
#  neon.layers.Affine(nout=100,init=init_norm, bias=neon.initializers.Constant(0),activation=activation),
#  neon.layers.Dropout(keep=0.5),
#  neon.layers.Affine(nout=50,init=init_norm, bias=neon.initializers.Constant(0),activation=activation),
#  neon.layers.Dropout(keep=0.5),  
  neon.layers.Affine(nout=25,init=init_norm, bias=neon.initializers.Constant(0),activation=activation),
  neon.layers.Dropout(keep=0.5),
  neon.layers.Affine(nout=10,init=init_norm, bias=neon.initializers.Constant(0),activation=activation),
#  neon.layers.Dropout(keep=0.5),
  neon.layers.Affine(nout=5,init=init_norm, bias=neon.initializers.Constant(0),activation=activation),
  neon.layers.Affine(nout=1,init=init_norm, bias=neon.initializers.Constant(0),activation=neon.transforms.Logistic())
#  neon.layers.Linear(nout=1,init=init_norm) 
  ]
mlp = neon.models.Model(layers=layers)
#callbacks = neon.callbacks.callbacks.Callbacks(mlp, eval_set=test,**args.callback_args)
def fnStop(state,val):
    if val<fnStop.val:
        fnStop.val = val
        fnStop.n = 0
    else:
        fnStop.n+=1
    return (state,fnStop.n>10)
fnStop.val=9999
fnStop.n=0
callbacks = neon.callbacks.callbacks.Callbacks(mlp, eval_set=test,**args.callback_args)
callbacks.add_early_stop_callback(fnStop)
mlp.fit(train, optimizer=optimizer, num_epochs=1000, cost=cost, callbacks=callbacks)

print "Saving model"
mlp.save_params("model.prm")

print "Metrics"
from sklearn import metrics
yh_train = mlp.get_outputs(train) 
print metrics.r2_score(y[tts],yh_train),metrics.mean_squared_error(y[tts],yh_train)
yh_test = mlp.get_outputs(test)
print metrics.r2_score(y[~tts],yh_test),metrics.mean_squared_error(y[~tts],yh_test)

print "Linear Model Metrics"
import sklearn.linear_model
clf = sklearn.linear_model.Ridge()
clf.fit(X[tts],y[tts])
yh_lm_train = clf.predict(X[tts])
print clf.score(X[tts],y[tts]),metrics.mean_squared_error(y[tts],yh_lm_train)
yh_lm_test = clf.predict(X[~tts])
print clf.score(X[~tts],y[~tts]),metrics.mean_squared_error(y[~tts],yh_lm_test)


print "Scatter Plots"
import matplotlib.pyplot as plt

plt.plot(y[~tts],yh_lm_test,".")
plt.xlabel("Affinity")
plt.ylabel("Model output")
plt.title("Model R^2: {:4.3f}".format(metrics.r2_score(y[~tts],yh_lm_test)))
plt.savefig("scatter_lm.png")
plt.show()

plt.plot(y[tts],yh_train,".")
plt.xlabel("Affinity")
plt.ylabel("Model output")
plt.title("Model R^2: {:4.3f}".format(metrics.r2_score(y[tts],yh_train)))
plt.savefig("scatter.png")
plt.show()

plt.plot(y[~tts],yh_test,".")
plt.xlabel("Affinity")
plt.ylabel("Model output")
plt.title("Model R^2: {:4.3f}".format(metrics.r2_score(y[~tts],yh_test)))
plt.savefig("scatter.png")
plt.show()

print "ROC Curves"
z = y.flatten()>(1-np.log(500)/np.log(50000))

fpr,tpr,th = metrics.roc_curve(z[~tts],yh_test.flatten())
auc = metrics.roc_auc_score(z[~tts],yh_test.flatten())
plt.plot(fpr,tpr)
plt.xlabel("false positive rate")
plt.ylabel("true positive rate")
plt.title("ANN ROC AUC: {:4.3f}".format(auc))
plt.savefig("ROC.png")
plt.show()

fpr,tpr,th = metrics.roc_curve(z[~tts],yh_lm_test.flatten())
auc = metrics.roc_auc_score(z[~tts],yh_lm_test.flatten())
plt.plot(fpr,tpr)
plt.xlabel("false positive rate")
plt.ylabel("true positive rate")
plt.title("LM ROC AUC: {:4.3f}".format(auc))
plt.savefig("ROC_lm.png")
plt.show()