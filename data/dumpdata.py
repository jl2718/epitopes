# -*- coding: utf-8 -*-
"""
Created on Wed Mar 30 16:15:31 2016

@author: johnlakness
"""

print "Fetching data from MySQL connector"
import mysql.connector
cnx = mysql.connector.connect(user='root', password='root',database='iedb_public')
cursor = cnx.cursor()
sql = """
select distinct
  mol1_seq epitope,
  left(sequence,180) mhc,
  as_num_value,
  units,
  as_char_value
from mhc_bind
join curated_epitope using(curated_epitope_id)
join  object on e_object_id=object_id
join  mhc_allele_restriction on DISPLAYED_RESTRICTION = MHC_ALLELE_NAME
join  source on chain_i_source_id=source_id
where char_length(mol1_seq)=9 
;
"""
cursor.execute(sql)
items = cursor.fetchall()
cursor.close()
cnx.close()

print "Dumping data to pickle file"
import pickle
with open("data.pkl","wb") as f:
  pickle.dump(items,f)

"""
select distinct
  mol1_seq epitope,
  left(sequence,180) mhc,
  coalesce(log(as_num_value),ml) as IC50,
  as_char_value
from mhc_bind
join curated_epitope using(curated_epitope_id)
join  object on e_object_id=object_id
join  mhc_allele_restriction on DISPLAYED_RESTRICTION = MHC_ALLELE_NAME
join  source on chain_i_source_id=source_id
join (select as_char_value,avg(log(as_num_value)) as ml from mhc_bind group by as_char_value) X using(as_char_value)
where char_length(mol1_seq)=9
limit 2048;
"""

"""
select distinct
  mol1_seq epitope,
  left(sequence,180) mhc,
  as_num_value,
  as_char_value
from mhc_bind
join curated_epitope using(curated_epitope_id)
join  object on e_object_id=object_id
join  mhc_allele_restriction on DISPLAYED_RESTRICTION = MHC_ALLELE_NAME
join  source on chain_i_source_id=source_id
join assay_type on assay_type_id=as_type_id
where char_length(mol1_seq)=9 
and units='nM' 
and as_num_value is not null
and as_char_value <> 'Positive'
;
"""