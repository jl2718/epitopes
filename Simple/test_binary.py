# -*- coding: utf-8 -*-
"""
Created on Wed Mar 30 13:47:21 2016

@author: johnlakness


Trains a convolutional neural network for epitope binding to MHC molecules from sequence data
Examples:
    python test.py -b gpu -e 10
        Run the example for 10 epochs of data using the nervana gpu backend
    python test.py --eval_freq 1
        After each training epoch the validation/test data set will be processed through the model
        and the cost will be displayed.
    python test.py --serialize 1 -s checkpoint.pkl
        After every iteration of training the model will be dumped to a pickle file named
        "checkpoint.pkl".  Changing the serialize parameter changes the frequency at which the
        model is saved.
    python test.py --model_file checkpoint.pkl
        Before starting to train the model, the model state is set to the values stored in the
        checkpoint file named checkpoint.pkl.
"""
import numpy as np
import pandas as pd
import itertools as it
import operator as op
import functools as ft

from neon.initializers import Gaussian,Constant
from neon.optimizers import GradientDescentMomentum,Adam
from neon.layers import Linear, Bias,Conv,Affine,Dropout
from neon.layers import GeneralizedCost
from neon.transforms import SumSquared,MeanSquared,Rectlin,Identity,SmoothL1Loss,Explin,Logistic,Tanh,LogLoss,CrossEntropyBinary
from neon.models import Model
from neon.callbacks.callbacks import Callbacks
from neon.util.argparser import NeonArgparser
from neon.backends import gen_backend
from neon.data import ArrayIterator

# parse the command line arguments
parser = NeonArgparser(__doc__)
args = parser.parse_args()

print("Fetching data from MySQL connector")
import mysql.connector
cnx = mysql.connector.connect(user='root', password='root',database='iedb_public')
cursor = cnx.cursor()
sql = """
select distinct
  mol1_seq epitope,
  left(sequence,180) mhc,
  as_char_value
from mhc_bind
join curated_epitope using(curated_epitope_id)
join  object on e_object_id=object_id
join  mhc_allele_restriction on DISPLAYED_RESTRICTION = MHC_ALLELE_NAME
join  source on chain_i_source_id=source_id
where char_length(mol1_seq)=9 
and as_char_value <> 'Positive'
;
"""
cursor.execute(sql)
items = cursor.fetchall()
cursor.close()
cnx.close()

print "Converting amino acid sequences"
aa = pd.read_table("../data/aminos.txt",header=None,sep="\s{2,}",engine='python').iloc[:,2]
A = np.zeros((len(items),189,20),dtype=np.int8) #initialize the one-hot array
reduce( # set the amino acid positions
  lambda x,l: A.itemset(l,1),
  [(i,j,a) for i,item in enumerate(items) for j,a in enumerate(aa.searchsorted(map(chr,item[0])+list(item[1])))],
  None)
X = A.view() # reshape without copy
X.shape = (len(items),189*20)
#X = np.array([[1 if i==ix else 0 for ix in aa.searchsorted(map(chr,item[0])+list(item[1])) for i in range(20)] for item in items])
bvd = {'Negative':0,'Positive-High':3,'Positive-Intermediate':2,'Positive-Low':1}
T = np.array([[bvd[item[2].decode('utf-8')]] for item in items]) >= 1

#X = np.random.rand(10000,189*20)
#y = np.random.randint(0,10,len(X))
tts = np.random.binomial(1,0.5,len(items))==1

print "Setting up Neon"
gen_backend(backend='cpu', batch_size=128)

train = ArrayIterator(X=X[tts,:], y=T[tts],lshape=(1,189,20),make_onehot=False)
test = ArrayIterator(X=X[~tts,:], y=T[~tts],lshape=(1,189,20),make_onehot=False)

print "Fitting model"
init_norm = Gaussian(loc=0.0, scale=0.001)
cost = GeneralizedCost(costfunc=CrossEntropyBinary())

#optimizer = GradientDescentMomentum(0.1, momentum_coef=0.9)
optimizer = GradientDescentMomentum(learning_rate = 0.01,momentum_coef=0.9)
layers = [
#  Conv((1,20,5),strides={'str_h':1,'str_w':1},init=init_norm, bias=Constant(0),activation=Identity(), name = "AAProps"),
#  Dropout(keep=0.5),  
#  Affine(nout=50,init=init_norm, bias=Constant(0),activation=Tanh(),name="Hidden-1"),
#  Dropout(keep=0.5),
#  Affine(nout=20,init=init_norm, bias=Constant(0),activation=Tanh(),name="Hidden-2"),
#  Dropout(keep=0.5),
#  Affine(nout=5,init=init_norm, bias=Constant(0),activation=Logistic(),name="Output")  
#  Dropout(keep=0.5),
  Affine(nout=1,init=init_norm, bias=Constant(0),activation=Logistic(),name="Output")
  #Linear(1, init=init_norm),
  #Bias(init=init_norm)
  ]
mlp = Model(layers=layers)
callbacks = Callbacks(mlp, eval_set=test,**args.callback_args)
mlp.fit(train, optimizer=optimizer, num_epochs=10, cost=cost,callbacks=callbacks)

yhat = mlp.get_outputs(test);yhat[:10]
print np.mean(np.abs(yhat-y[~tts]))

import matplotlib.pyplot as plt
num_bins = 20
counts, bin_edges = np.histogram(yhat, bins=num_bins, normed=True)
cdf = np.cumsum(counts)
plt.plot(bin_edges[1:], cdf)

for l in mlp.layers.layers:
  print l
#print mlp.layers.layers[0].W.get().shape
#print mlp.layers.layers[3].W.get().shape

print "Saving model"
mlp.save_params("epitope_model.prm")