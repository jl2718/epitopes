# -*- coding: utf-8 -*-
"""
Created on Wed Mar 30 13:47:21 2016

@author: johnlakness


Trains a convolutional neural network for epitope binding to MHC molecules from sequence data
Examples:
    python test.py -b gpu -e 10
        Run the example for 10 epochs of data using the nervana gpu backend
    python test.py --eval_freq 1
        After each training epoch the validation/test data set will be processed through the model
        and the cost will be displayed.
    python test.py --serialize 1 -s checkpoint.pkl
        After every iteration of training the model will be dumped to a pickle file named
        "checkpoint.pkl".  Changing the serialize parameter changes the frequency at which the
        model is saved.
    python test.py --model_file checkpoint.pkl
        Before starting to train the model, the model state is set to the values stored in the
        checkpoint file named checkpoint.pkl.
"""
import numpy as np
import pandas as pd
import itertools as it
import operator as op
import functools as ft

from neon.initializers import Gaussian,Constant
from neon.optimizers import GradientDescentMomentum,Adam
from neon.layers import Linear, Bias,Conv,Affine,Dropout
from neon.layers import GeneralizedCost
from neon.transforms import SumSquared,MeanSquared,Rectlin,Identity,SmoothL1Loss,Explin,Logistic,Tanh,LogLoss,CrossEntropyBinary
from neon.models import Model
from neon.callbacks.callbacks import Callbacks
from neon.util.argparser import NeonArgparser
from neon.backends import gen_backend
from neon.data import ArrayIterator

# parse the command line arguments
parser = NeonArgparser(__doc__)
args = parser.parse_args()

print("Fetching data from MySQL connector")
import mysql.connector
cnx = mysql.connector.connect(user='root', password='root',database='iedb_public')
cursor = cnx.cursor()
sql = """
select distinct
  mol1_seq epitope,
  as_char_value
from mhc_bind
join curated_epitope using(curated_epitope_id)
join  object on e_object_id=object_id
where char_length(mol1_seq)=9 
and as_char_value <> 'Positive'
and mhc_allele_name = 'HLA-A*02:01'
;
"""
cursor.execute(sql)
items = cursor.fetchall()
cursor.close()
cnx.close()

print "Converting amino acid sequences"
aa = pd.read_table("../data/aminos.txt",header=None,sep="\s{2,}",engine='python').iloc[:,2]
A = np.zeros((len(items),9,20),dtype=np.int8) #initialize the one-hot array
reduce( # set the amino acid positions
  lambda x,l: A.itemset(l,1),
  [(i,j,a) for i,item in enumerate(items) for j,a in enumerate(aa.searchsorted(map(chr,item[0])))],
  None)
X = A.view() # reshape without copy
X.shape = (len(items),9*20)
labels = ['Negative','Positive-Low','Positive-Intermediate','Positive-High']
bvd = dict(zip(labels,range(4)))#{'Negative':0,'Positive-High':3,'Positive-Intermediate':2,'Positive-Low':1}
z = np.array([[bvd[item[1].decode('utf-8')]] for item in items])
y = z/3.0
#X = np.random.rand(10000,189*20)
#y = np.random.randint(0,10,len(X))
tts = np.random.binomial(1,0.5,len(items))==1

print "Setting up Neon"
gen_backend(backend='cpu', batch_size=32)

train = ArrayIterator(X=X[tts,:], y=y[tts],lshape=(1,9,20), make_onehot=False)
test = ArrayIterator(X=X[~tts,:], y=y[~tts],lshape=(1,9,20), make_onehot=False)

print "Fitting model"
init_norm = Gaussian(loc=0.0, scale=0.01)
cost = GeneralizedCost(costfunc=SumSquared())

#optimizer = GradientDescentMomentum(0.01, momentum_coef=0.5)
optimizer = Adam()
activation = Tanh()
layers = [
  Conv((1,20,20),strides={'str_h':1,'str_w':20},init=init_norm, bias=Constant(0),activation=Identity(), name = "AAProps"),
  Dropout(keep=0.5),  
  Affine(nout=200,init=init_norm, bias=Constant(0),activation=activation),
  Dropout(keep=0.5),
  Affine(nout=100,init=init_norm, bias=Constant(0),activation=activation),
  Dropout(keep=0.5),
  Affine(nout=50,init=init_norm, bias=Constant(0),activation=activation),
  Dropout(keep=0.5),
  Affine(nout=25,init=init_norm, bias=Constant(0),activation=activation),
#  Dropout(keep=0.5),
  Affine(nout=10,init=init_norm, bias=Constant(0),activation=activation),
#  Dropout(keep=0.5),
  Affine(nout=5,init=init_norm, bias=Constant(0),activation=activation),
#  Dropout(keep=0.5),
  Affine(nout=1,init=init_norm, bias=Constant(0),activation=activation),
#  Dropout(keep=0.5),
#  Affine(nout=1,init=init_norm, bias=Constant(0),activation=Logistic(),name="Output")  
#  Linear(1, init=Constant(np.max(y)/2.0)),
#  Bias(init=Constant(np.mean(y)))
  ]
mlp = Model(layers=layers)
callbacks = Callbacks(mlp, eval_set=test,**args.callback_args)
mlp.fit(train, optimizer=optimizer, num_epochs=1000, cost=cost, callbacks=callbacks)
print "Saving model"
mlp.save_params("epitope_model.prm")

#mlp = Model("epitope_model.prm")
#yhat = mlp.get_outputs(train)
#print np.mean((yhat-T[tts])**2)

#for l in mlp.layers.layers:
#  print l
#print mlp.layers.layers[0].W.get()
print "Metrics"
from sklearn import metrics
yh_train = mlp.get_outputs(train) 
print metrics.r2_score(y[tts],yh_train),metrics.mean_squared_error(y[tts],yh_train)
yh_test = mlp.get_outputs(test)
print metrics.r2_score(y[~tts],yh_test),metrics.mean_squared_error(y[~tts],yh_test)
print "Linear Model Metrics"
import sklearn.linear_model
clf = sklearn.linear_model.Ridge()
clf.fit(X[tts],y[tts])
yh_lm_train = clf.predict(X[tts])
print clf.score(X[tts],y[tts]),metrics.mean_squared_error(y[tts],yh_lm_train)
yh_lm_test = clf.predict(X[~tts])
print clf.score(X[~tts],y[~tts]),metrics.mean_squared_error(y[~tts],yh_lm_test)


import  matplotlib.pyplot as plt
#plt.plot(y[~tts],yh_test,".")
#G_train = [[yh for yo,yh in g] for k,g in it.groupby(sorted(zip(y[tts],yh_train)),op.itemgetter(0))]
#plt.violinplot(G_train)
G_test = [[yh for yo,yh in g] for k,g in it.groupby(sorted(zip(y[~tts],yh_test)),op.itemgetter(0))]
plt.violinplot(G_test,vert=False)
ax = plt.subplot(111)
ax.set_yticks(range(1,5))
ax.set_yticklabels(labels)
#G_lm = [[yh for yo,yh in g] for k,g in it.groupby(sorted(zip(y[~tts],yh_lm_test)),op.itemgetter(0))]
#plt.violinplot(G_lm)
plt.xlabel("model output")
plt.title("Distribution of model output by binding affinity")
plt.savefig("violin.png")
plt.show()

D = np.zeros((len(yh_test),4))
zx = sorted(zip(yh_test.reshape((len(yh_test))).tolist(),z[~tts]))
reduce(lambda o,(i,(yh,k)):D.itemset((i,k),1),enumerate(zx))
#for i,(yh,k) in enumerate(yx):
#  D.itemset((i,k),1)

cD = np.transpose(np.cumsum(D,axis=0))
plt.stackplot([yh for yh,x in zx],cD/cD.sum(axis=0)) 
crD = np.transpose(np.cumsum(D[::-1],axis=0)[::-1])
plt.stackplot([yh for yh,x in zx],crD/crD.sum(axis=0))
plt.show()

ccrD = np.cumsum(crD,axis=0)
crcrD = np.cumsum(crD[::-1],axis=0)[::-1]

tpr = crcrD/np.array(crcrD[:,0],ndmin=2).transpose()
fpr = ccrD/np.array(ccrD[:,0],ndmin=2).transpose()
auc = [metrics.roc_auc_score(z[~tts]>c,yh_test) for c in range(3)]
labels = ['Negative','Positive-Low','Positive-Intermediate','Positive-High']
for c in range(3):
  plt.plot(fpr[c],tpr[c+1],label="{}: {:4.3f}".format(labels[c+1],auc[c]))
  #plt.plot(*metrics.roc_curve(y[~tts]>i,yh_test))#,label="{}: {:4.3f}".format(labels[i+1],auc[i]))
plt.legend(loc = 'lower right',title = "Binding Threshold: AUC")
plt.xlabel("false positive rate")
plt.ylabel("true positive rate")
plt.title("ROC curves by threshold")
plt.savefig("ROC.png")
plt.show()

